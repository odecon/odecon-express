import bodyParser from "body-parser"
import cors from "cors"
import express, { Request, Response, NextFunction, RequestHandler, Application } from "express"
import morgan from "morgan"
import Database from "../db"
import ProcessEnv = NodeJS.ProcessEnv
import * as dotenv from "dotenv"
import { HttpException } from "@odecon/odecon-common"
import { IController } from "../controllers/interfaces"
import { errorMiddleware } from "../middewares/error.middleware"
import { Logger } from "@odecon/odecon-common"
import { IDbConfig } from "../db/IDbConfig"

export class App {
    public app: Application
    public rootPath: string
    private db!: Database
    private logger: Logger = new Logger()

    public constructor(controllers: IController[], rootPath = "/") {
        this.app = express()
        this.rootPath = rootPath

        dotenv.config()

        this.connectToDatabase()
        this.initializeMiddleware([cors(), morgan("dev"), bodyParser.json()])
        this.initializeControllers(controllers)
        this.initializeDefaultHandler()
    }

    public listen = (): void => {
        const port = process.env.PORT || 8080
        this.app.listen(port, () => {
            this.logger.logSuccess(`App listening on port ${port}`)
        })
    }

    private connectToDatabase = (): void => {
        const { DB_URL, DB_NAME, DB_USER, DB_PASS }: IDbConfig = process.env

        this.db = new Database({ DB_NAME, DB_URL, DB_USER, DB_PASS })
        this.db.connect()
    }

    private initializeMiddleware = (middlewares: RequestHandler[]): void => {
        if (middlewares) {
            middlewares.map((middleware) => this.app.use(middleware))
        }
    }

    private initializeControllers = (controllers: IController[]): void => {
        controllers.forEach((controller) => {
            this.app.use(this.rootPath, controller.router)
        })
    }

    private initializeDefaultHandler(): void {
        this.app.use((req: Request, res: Response, next: NextFunction) => {
            next(
                new HttpException({
                    message: "Not found",
                    description: `${req.method} '${req.originalUrl}' was not found.`,
                    status: 404,
                }),
            )
        }, errorMiddleware)
    }
}
