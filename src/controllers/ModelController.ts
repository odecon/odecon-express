import { Router } from "express"
import { QueryPopulateOptions } from "mongoose"
import { BaseModelType } from "../schema"
import { Logger } from "@odecon/odecon-common"
import {
    addOneRequestHandler,
    deleteOneRequestHandler,
    findManyRequestHandler,
    findOneRequestHandler,
    updateOneRequestHandler,
} from "./request-handlers"
import { IController } from "./interfaces"

export abstract class ModelController<T extends BaseModelType> implements IController {
    public populateFields: (string | QueryPopulateOptions)[]
    public router: Router = Router()
    public logger = new Logger()

    public abstract path: string
    public abstract model: T
    public abstract displayName?: string
    public abstract collectionName?: string

    protected constructor(populateFields: (string | QueryPopulateOptions)[] = []) {
        this.populateFields = populateFields
    }

    public abstract init(): void

    public addOne = addOneRequestHandler.bind(this)
    public findOne = findOneRequestHandler.bind(this)
    public findMany = findManyRequestHandler.bind(this)
    public deleteOne = deleteOneRequestHandler.bind(this)
    public updateOne = updateOneRequestHandler.bind(this)
}
