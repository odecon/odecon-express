import { NextFunction, Request, Response } from "express"
import { DocumentType } from "@typegoose/typegoose"
import { HttpException } from "@odecon/odecon-common"
import { ModelController } from "../ModelController"
import { BaseModelType } from "../../schema"

export function deleteOneRequestHandler<T>(
    this: ModelController<BaseModelType>,
    req: Request,
    res: Response,
    next: NextFunction,
): void {
    const {
        params: { id },
    } = req

    this.model
        .findByIdAndDelete(id)
        .exec()
        .then((removedDocument?: DocumentType<T> | null) => {
            res.sendStatus(200)
        })
        .catch((err) => {
            this.logger.logError("CONTROLLER:REMOVE:" + err)
            next(
                new HttpException({
                    message: `En feil oppstod ved sletting av ${this.displayName}`,
                }),
            )
        })
}
