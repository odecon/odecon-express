import { NextFunction, Request, Response } from "express"
import { DocumentType } from "@typegoose/typegoose"
import { HttpException } from "@odecon/odecon-common"
import { ModelController } from "../ModelController"
import { BaseModelType } from "../../schema"

export function findOneRequestHandler<T>(
    this: ModelController<BaseModelType>,
    req: Request,
    res: Response,
    next: NextFunction,
): void {
    const {
        params: { id },
    } = req
    this.model
        .findById(id)
        .populate(this.populateFields.join(" "))
        .exec()
        .then((foundDocument: DocumentType<T> | null) => {
            return {
                data: foundDocument,
            }
        })
        .then((results) => {
            res.status(200).json({ ...results })
        })
        .catch((err) => {
            this.logger.logError("CONTROLLER:FIND_ONE:" + err)
            next(new HttpException({ message: `En feil oppstod ved uthenting av ${this.displayName}` }))
        })
}
