import { NextFunction, Request, Response } from "express"
import { DocumentType } from "@typegoose/typegoose"
import { HttpException } from "@odecon/odecon-common"
import { ModelController } from "../ModelController"
import { BaseModelType } from "../../schema"

export function findManyRequestHandler<T>(
    this: ModelController<BaseModelType>,
    req: Request,
    res: Response,
    next: NextFunction,
): void {
    const filters = getFiltersFromRequest(req)
    const { page, count, search } = getFindManyQueryFromRequest(req)
    const skips = count * page
    this.model.getQuery(search).then((searchQuery) => {
        return this.model
            .find({ ...filters, ...searchQuery })
            .skip(skips)
            .limit(count)
            .populate(this.populateFields.join(" "))
            .exec()
            .then((foundDocuments: DocumentType<T>[]) => {
                res.status(200).json({
                    data: foundDocuments,
                    meta: {
                        page,
                        count,
                    },
                })
            })
            .catch((err) => {
                this.logger.logError("CONTROLLER:FIND_ALL:" + err)
                next(new HttpException({ message: `En feil oppstod ved uthentingen av alle ${this.displayName}` }))
            })
    })
}

const getFindManyQueryFromRequest = (
    req: Request<unknown, unknown, unknown, { page?: string; count?: string; search?: string }>,
): { page: number; count: number; search?: string } => {
    return {
        page: parseInt(req.query.page, 10) | 0,
        count: parseInt(req.query.count, 10) | 50,
        search: req.query.search,
    }
}

const getFiltersFromRequest = (req: Request<unknown, unknown, unknown>) => {
    const filters = { ...req.query }
    delete filters.search
    delete filters.page
    delete filters.count
    return filters
}
