import { NextFunction, Request, Response } from "express"
import { DocumentType } from "@typegoose/typegoose"
import { HttpException } from "@odecon/odecon-common"
import { ModelController } from "../ModelController"
import { BaseModelType } from "../../schema"
import { flattenObject } from "../../helpers"

export function updateOneRequestHandler<T>(
    this: ModelController<BaseModelType>,
    req: Request,
    res: Response,
    next: NextFunction,
): void {
    const {
        params: { id },
        body,
    } = req
    const updatingObject = flattenObject(body)
    this.model
        .findOneAndUpdate({ _id: id }, { ...updatingObject }, { new: true })
        .exec()
        .then((updatedDocument: DocumentType<T> | null) => {
            res.status(200).json({
                data: updatedDocument,
            })
        })
        .catch((err) => {
            this.logger.logError("CONTROLLER:UPDATE:" + err)
            next(new HttpException({ message: `En feil oppstod ved oppdatering av ${this.displayName}` }))
        })
}
