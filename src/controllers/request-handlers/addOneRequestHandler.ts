import { NextFunction, Request, Response } from "express"
import { DocumentType } from "@typegoose/typegoose"
import { HttpException } from "@odecon/odecon-common"
import { ModelController } from "../ModelController"
import { BaseModelType } from "../../schema"

export function addOneRequestHandler<T>(
    this: ModelController<BaseModelType>,
    req: Request,
    res: Response,
    next: NextFunction,
): void {
    const { body } = req
    const document = new this.model(body)
    document
        .save()
        .then((savedDocument: DocumentType<T>) => {
            let field
            for (field of this.populateFields) {
                savedDocument.populate(field)
            }
            return savedDocument.execPopulate()
        })
        .then((savedDocument: DocumentType<T>) => {
            res.locals.populatedDocument = { ...savedDocument.toJSON() }
            res.status(200).json({
                data: savedDocument,
            })
        })
        .catch((err) => {
            this.logger.logError("CONTROLLER:ADD:" + err)
            next(new HttpException({ message: `En feil oppstod ved opprettelse av en ny ${this.displayName}` }))
        })
}
