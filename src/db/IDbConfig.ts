export interface IDbConfig {
    DB_URL?: string
    DB_NAME?: string
    DB_USER?: string
    DB_PASS?: string
}
