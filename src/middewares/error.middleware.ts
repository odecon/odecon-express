import { NextFunction, Request, Response } from "express"
import { HttpException } from "@odecon/odecon-common"

export const errorMiddleware = (error: HttpException, req: Request, res: Response, next: NextFunction): void => {
    const status = error.status || 500
    const message = error.message || "Ups! Her skjedde det noe rart..."
    const description = error.message
        ? error.description
        : "Vennligst kontakt utvikleren av denne applikasjonen for ytterligere hjelp."
    const fields = error.fields

    res.status(status).json({
        error: {
            message,
            description,
            fields,
        },
    })
    next()
}
