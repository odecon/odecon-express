export const flattenObject = (obj: Record<string, unknown>): Record<string, unknown> => {
    const flattenedObj = {}

    Object.keys(obj).forEach((key) => {
        if (typeof obj[key] === "object" && obj[key] !== null) {
            Object.assign(flattenedObj, flattenObject(obj[key] as Record<string, unknown>))
        } else {
            flattenedObj[key] = obj[key]
        }
    })
    return flattenedObj
}
